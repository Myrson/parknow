package com.example.parknow;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyLog;
import com.example.parknow.requests.ConfrimArrivalHandler;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import java.net.InetAddress;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity{

    NavController navController;
    BottomNavigationView bottomNavigationView;

    RelativeLayout reltimer;
    TextView timer;
    private CountDownTimer countDownTimer;
    final private long timerTime = 15 * 1000 * 60;
    private long timeLeftInMilliseconds = timerTime;
    ImageButton cancelbtn;
    ImageButton confirmbtn;
    private String idReservation;
    public boolean isCurrentReservation = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


            timer = (TextView) findViewById(R.id.timer);
            reltimer = (RelativeLayout) findViewById(R.id.reltimer);
            cancelbtn = (ImageButton) findViewById(R.id.cancel_btn);
            confirmbtn = (ImageButton) findViewById(R.id.confirm_btn);

            //Bottom Navigation
            bottomNavigationView = findViewById(R.id.nav_view);
            AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                    R.id.navigation_map, R.id.navigation_history, R.id.navigation_account).build();
            navController = Navigation.findNavController(this, R.id.nav_host_fragment);
            NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
            NavigationUI.setupWithNavController(bottomNavigationView, navController);

            //Cancel
            cancelbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendReservationStatusChange(idReservation, v, "cancel");
                    countDownTimer.cancel();
                    TimerToDeafultValues();
                }
            });

            //Confirm
            confirmbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendReservationStatusChange(idReservation, v, "done");
                    countDownTimer.cancel();
                    TimerToDeafultValues();
                }
            });


    }

    private  void sendReservationStatusChange(String id, View v, String status){
        ConfrimArrivalHandler confrimArrivalHandler = new ConfrimArrivalHandler(id);
        confrimArrivalHandler.status = status;

            boolean didRes = false;
            try {
                didRes =  confrimArrivalHandler.execute().get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            if (didRes) {
                Toast.makeText(v.getContext(), "Arrival "+ status+ ".", Toast.LENGTH_LONG).show();
            }
    }

    public void startTimer(String idReservation){
        if(isCurrentReservation == false){
            isCurrentReservation = true;
            this.idReservation = idReservation;
            countDownTimer = new CountDownTimer(timeLeftInMilliseconds, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    timeLeftInMilliseconds = millisUntilFinished;
                    updateTimer();
                }

                @Override
                public void onFinish() {
                    SetFailReservation(getWindow().getDecorView().getRootView());
                    Log.e(VolleyLog.TAG,"Reservation finish.");
                    TimerToDeafultValues();
                }


            }.start();
            SetVisibleTimerFields(View.VISIBLE);
        }
        else{
            Toast.makeText(MainActivity.this, "Your previous booking has not finished yet.", Toast.LENGTH_LONG).show();
        }
    }

    private void TimerToDeafultValues() {
        SetVisibleTimerFields(View.INVISIBLE);
        timeLeftInMilliseconds = timerTime;
        isCurrentReservation = false;
    }

    private void SetFailReservation(View v){
         sendReservationStatusChange(this.idReservation,v, "fail");
     }
    private void SetVisibleTimerFields(int visible) {
        timer.setVisibility(visible);
        reltimer.setVisibility(visible);
        confirmbtn.setVisibility(visible);
        cancelbtn.setVisibility(visible);

    }

/*
    public void startStop(){
        if(timeRunning){
            stopTimer();
        } else {
            startTimer();
        }
    }

    public void stopTimer(){
        countDownTimer.cancel();
        //timer.setText("START");
        timeRunning = true;
    }
 */

    public void updateTimer(){
        int minutes = (int) timeLeftInMilliseconds / 60000;
        int seconds = (int) timeLeftInMilliseconds % 60000 / 1000;

        String timeLeftText;

        timeLeftText = "" + minutes;
        timeLeftText += ":";
        if(seconds < 10) timeLeftText += "0";
        timeLeftText += seconds;

        timer.setText(timeLeftText);
    }




}