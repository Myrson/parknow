package com.example.parknow.requests;

import android.os.AsyncTask;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ConfrimArrivalHandler extends AsyncTask<Void, Void, Boolean > {
    private OkHttpClient client = new OkHttpClient();

    private String bookingID;
    public String status = "done";

    public ConfrimArrivalHandler(String bookingID) {
            this.bookingID = bookingID;
    }



    @Override
    protected Boolean doInBackground(Void... params) {
        FormBody formBody = new FormBody.Builder()
                .add("status", status)
                .build();

        String reservationURL = "https://parknow.azurewebsites.net/api/Booking/" + bookingID;
        Request request = new Request.Builder()
                .url(reservationURL)
                .put(formBody)
                .build();
        try {
            Response response = client.newCall(request).execute();

            if (!response.isSuccessful()) {

                if(response.code() == 400){
                   if( response.body().toString()== "Time is up. Reservation fail."){

                       return false;
                   }
                }
                else throw new IOException("Unexpected code " + response.toString());

            }

            System.out.println("--- " + response.code());

            return response.isSuccessful();
        } catch (Exception e) {
            System.out.println("--- Exception in confirm arrival get method");
        }
        return false;
    }



}