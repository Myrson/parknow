package com.example.parknow.requests;

import android.os.AsyncTask;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class RegisterUserHandler extends AsyncTask<String, Void, Boolean> {
    private OkHttpClient client = new OkHttpClient();
    private String email, name, surname, birthday, passwordHash;

    public RegisterUserHandler(String email, String name, String surname, String birthday, String passwordHash) {
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
        this.passwordHash = passwordHash;
    }

    @Override
    protected Boolean doInBackground(String... params) {
        FormBody formBody = new FormBody.Builder()
                .add("email", email)
                .add("name", name)
                .add("surname", surname)
                .add("birthday", birthday)
                .add("passwordHash", passwordHash)
                .build();

        String UserURL = "https://parknow.azurewebsites.net/api/user";
        Request request = new Request.Builder()
                .url(UserURL)
                .post(formBody)
                .build();

        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful())
                throw new IOException("Unexpected code " + response.toString());
            System.out.println("--- " + response.code());
            return response.isSuccessful();
        } catch (Exception e) {
            System.out.println("--- Exception in register post method");
        }
        return null;
    }
}
