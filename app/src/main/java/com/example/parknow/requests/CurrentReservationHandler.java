package com.example.parknow.requests;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.example.parknow.models.Reservation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class CurrentReservationHandler extends AsyncTask<Integer, Void, Boolean> {
    private OkHttpClient client = new OkHttpClient();
    private Reservation reservationModel;
    private Context contextHist;
    private  String resposne;

    public ArrayList<Reservation> reservationsHistory;

    public CurrentReservationHandler(Context context) {
        this.contextHist = context;
    }

    static ProgressDialog pd_ring;


    @Override
    protected Boolean doInBackground(Integer... params) {
       
        String UserURL = "https://parknow.azurewebsites.net/api/bookings/user/" + params[0];
        Request request = new Request.Builder()
                .url(UserURL)
                .build();
        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful())
                throw new IOException("Unexpected code " + response.toString());
            System.out.println("--- " + response.code());
            resposne =  response.body().string();
            reservationsHistory = parseReservation(resposne);
            return response.isSuccessful();
        } catch (Exception e) {
            System.out.println("--- Exception in history get method");
        }
        return null;
    }


    public static ArrayList<Reservation> parseReservation(String data) {

        ArrayList<Reservation> array = new ArrayList<>();

        try {

            JSONArray jsonArr = new JSONArray(data);

            for (int i=0; i < jsonArr.length(); i++) {
                JSONObject obj = jsonArr.getJSONObject(i);
                Reservation reservation = new Reservation();

                reservation.setId(obj.getString("id"));
                reservation.setStatus(obj.getString("status"));

                reservation.setSpaceId(obj.getString("spaceId"));
                reservation.setParkingId(obj.getString("parkingId"));


                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

                String startTimeString = obj.getString("startTime");
                Date startTime = sdf.parse(startTimeString);
                reservation.setStartTime( startTime);


                array.add(reservation);
            }
            return array;
        }
        catch(JSONException ex) {
            ex.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


}