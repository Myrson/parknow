package com.example.parknow.requests;

import android.os.AsyncTask;

import com.example.parknow.models.Parking;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class GetParkingHandler extends AsyncTask<String, Void, Boolean> {
    private OkHttpClient client = new OkHttpClient();

    public ArrayList<Parking> parkings;

    @Override
    protected Boolean doInBackground(String... params) {

        final Request request = new Request.Builder().url("https://parknow.azurewebsites.net/api/parking").build();

        try {
            Response response = client.newCall(request).execute();
            parkings = parseParkings(response.body().string());
            if (!response.isSuccessful())
                throw new IOException("Unexpected code " + response.toString());
            return response.isSuccessful();
        } catch (Exception e) {
            System.out.println("--- Exception in login post method");
        }
        return null;
    }

    private static ArrayList<Parking> parseParkings(String data) {
        ArrayList<Parking> array = new ArrayList<>();

        try {
            JSONArray jsonArr = new JSONArray(data);
            ObjectMapper objectMapper = new ObjectMapper();

            for (int i=0; i < jsonArr.length(); i++) {
                JSONObject obj = jsonArr.getJSONObject(i);
                Parking parking = objectMapper.readValue(obj.toString(), Parking.class);
                array.add(parking);
            }

            return array;
        }
        catch(JSONException ex) {
            ex.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
