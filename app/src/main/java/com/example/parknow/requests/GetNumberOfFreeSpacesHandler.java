package com.example.parknow.requests;

import android.os.AsyncTask;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

public class GetNumberOfFreeSpacesHandler extends AsyncTask<Void, Void, String> {
private OkHttpClient client = new OkHttpClient();
private int idParking;

public GetNumberOfFreeSpacesHandler(int idParking) {
        this.idParking = idParking;
        }

@Override
protected String doInBackground(Void... params) {

        final Request request = new Request.Builder().url("https://parknow.azurewebsites.net/api/Space/free/" + idParking).build();

        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful())
            throw new IOException("Unexpected code " + response.toString());
            String c= response.body().string();
            return c;
        } catch (Exception e) {
        System.out.println("--- Exception in get fres spaces number get method");
        }
        return null;
        }
}
