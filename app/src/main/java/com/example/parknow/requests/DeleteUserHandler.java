package com.example.parknow.requests;

import android.os.AsyncTask;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class DeleteUserHandler extends AsyncTask<String, Void, Boolean> {
    private OkHttpClient client = new OkHttpClient();
    private int id;

    public DeleteUserHandler(int id) {
        this.id = id;
    }

    @Override
    protected Boolean doInBackground(String... params) {
        String UserURL = "https://parknow.azurewebsites.net/api/user/" + id;
        Request request = new Request.Builder()
                .url(UserURL)
                .delete()
                .build();

        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful())
                throw new IOException("Unexpected code " + response.toString());
            return response.isSuccessful();
        } catch (Exception e) {
            System.out.println("--- Exception in login post method");
        }
        return false;
    }
}
