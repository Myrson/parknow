package com.example.parknow.requests;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.example.parknow.models.Reservation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ReservationHandler extends AsyncTask<Void, Void, String > {
    private OkHttpClient client = new OkHttpClient();

    private String userId;
    private String parkingId;

    public ReservationHandler(String userId, String parkingId) {
            this.userId = userId;
            this.parkingId = parkingId;
    }



    @Override
    protected String doInBackground(Void... params) {
        FormBody formBody = new FormBody.Builder()
                .add("userId", userId)
                .add("parkingId", parkingId)
                .build();

        String reservationURL = "https://parknow.azurewebsites.net/api/Booking";
        Request request = new Request.Builder()
                .url(reservationURL)
                .post(formBody)
                .build();
        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful())
                throw new IOException("Unexpected code " + response.toString());

            System.out.println("--- " + response.code());
            String resposne =  response.body().string();

            return resposne;
        } catch (Exception e) {
            System.out.println("--- Exception in reservation  get method");
        }
        return null;
    }



}