package com.example.parknow.requests;

import android.os.AsyncTask;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ChangeUserHandler extends AsyncTask<String, Void, Boolean> {
    private OkHttpClient client = new OkHttpClient();
    private int id;
    private String name, surname, password;

    public ChangeUserHandler(int id, String name, String surname, String password) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.password = password;
    }

    @Override
    protected Boolean doInBackground(String... params) {
        FormBody formBody = new FormBody.Builder()
                .add("name", name)
                .add("surname", surname)
                .add("passwordHash", password)
                .build();

        String UserURL = "https://parknow.azurewebsites.net/api/user/" + id;
        Request request = new Request.Builder()
                .url(UserURL)
                .put(formBody)
                .build();

        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful())
                throw new IOException("Unexpected code " + response.toString());
            return response.isSuccessful();
        } catch (Exception e) {
            System.out.println("--- Exception in login post method");
        }
        return false;
    }
}
