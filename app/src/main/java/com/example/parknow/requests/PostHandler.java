package com.example.parknow.requests;

import android.os.AsyncTask;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class PostHandler extends AsyncTask<String, Void, String> {
    private OkHttpClient client = new OkHttpClient();
    private String email, password;

    public PostHandler(String email, String password) {
        this.email = email;
        this.password = password;
    }

    @Override
    protected String doInBackground(String... params) {
        FormBody formBody = new FormBody.Builder()
                .add("email", email)
                .add("passwordHash", password)
                .build();

        String UserURL = "https://parknow.azurewebsites.net/api/user";
        Request request = new Request.Builder()
                .url(UserURL + "/login")
                .post(formBody)
                .build();

        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful())
                throw new IOException("Unexpected code " + response.toString());
            return response.body().string();
        } catch (Exception e) {
            System.out.println("--- Exception in login post method");
        }
        return null;
    }
}
