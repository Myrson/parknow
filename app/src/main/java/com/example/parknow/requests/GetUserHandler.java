package com.example.parknow.requests;

import android.os.AsyncTask;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class GetUserHandler extends AsyncTask<String, Void, String> {
    private OkHttpClient client = new OkHttpClient();
    private int id;

    public GetUserHandler(int id) {
        this.id = id;
    }

    @Override
    protected String doInBackground(String... params) {

        final Request request = new Request.Builder().url("https://parknow.azurewebsites.net/api/user/" + id).build();

        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful())
                throw new IOException("Unexpected code " + response.toString());
            return response.body().string();
        } catch (Exception e) {
            System.out.println("--- Exception in login post method");
        }
        return null;
    }
}
