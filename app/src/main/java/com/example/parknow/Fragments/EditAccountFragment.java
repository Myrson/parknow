package com.example.parknow.Fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.parknow.Fragments.login.LoginActivity;
import com.example.parknow.R;
import com.example.parknow.models.User;
import com.example.parknow.requests.ChangeUserHandler;
import com.example.parknow.requests.DeleteUserHandler;
import com.example.parknow.requests.GetUserHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.concurrent.ExecutionException;

public class EditAccountFragment extends Fragment {

    private int userId;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_edit_account, container, false);

        userId = getUserId();

        final EditText name = root.findViewById(R.id.edit_name);
        final EditText surname = root.findViewById(R.id.txtsur);
        final EditText password = root.findViewById(R.id.txtpass);

        GetUserHandler handler = new GetUserHandler(getUserId()); // TODO change
        try {
            String res = handler.execute().get();

            ObjectMapper objectMapper = new ObjectMapper();
            User user = objectMapper.readValue(res, User.class);

            name.setText(user.getName());
            surname.setText(user.getSurname());
            password.setText(user.getPasswordHash());

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        //Button List
        final Button saveChangesBtn = root.findViewById(R.id.btn_save_changes);
        saveChangesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeUser(name.getText().toString(), surname.getText().toString(), password.getText().toString());
            }
        });

        final Button deleteAccountBtn = root.findViewById(R.id.btn_delete_account);
        deleteAccountBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteUser();
            }
        });

        return root;
    }

    private void deleteUser() {
        DeleteUserHandler handler = new DeleteUserHandler(userId);
        boolean didDelete = false;
        try {
            didDelete = handler.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        if (didDelete) {
            showSuccess();

            SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            editor.remove("user_id"); // Storing integer
            editor.commit(); // commit changes

            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivity(intent);
        } else {
            showFailedChange();
        }
    }

    private void changeUser(final String name, final String surname, final String password) {

        if(name.isEmpty() || surname.isEmpty() || password.isEmpty()) {
            showEmptyFields();
            return;
        }

        ChangeUserHandler handler = new ChangeUserHandler(userId, name, surname, password);
        boolean didChangeUser = false;
        try {
            didChangeUser = handler.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        if (didChangeUser) {
            showSuccess();

            Fragment fragment = null;
            fragment = new AccountFragment();
            replaceFragment(fragment);
            replaceFragment(fragment);
        } else {
            showFailedDelete();
        }
    }

    private void replaceFragment(Fragment mapFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, mapFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private int getUserId() {
        SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        return pref.getInt("user_id", -1); // getting Integer
    }

    private void showEmptyFields() {
        Toast.makeText(getActivity().getApplicationContext(), "Fields cannot be empty", Toast.LENGTH_SHORT).show();
    }

    private void showFailedChange() {
        Toast.makeText(getActivity().getApplicationContext(), "Failed to change account data", Toast.LENGTH_SHORT).show();
    }

    private void showSuccess() {
        Toast.makeText(getActivity().getApplicationContext(), "Your data has been changed", Toast.LENGTH_SHORT).show();
    }

    private void showFailedDelete() {
        Toast.makeText(getActivity().getApplicationContext(), "Failed to delete the account", Toast.LENGTH_SHORT).show();
    }
}
