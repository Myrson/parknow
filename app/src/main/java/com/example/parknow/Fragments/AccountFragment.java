package com.example.parknow.Fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.parknow.Fragments.login.LoginActivity;
import com.example.parknow.R;
import com.example.parknow.models.User;
import com.example.parknow.requests.GetUserHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;

public class AccountFragment extends Fragment {


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_account, container, false);

        TextView name = root.findViewById(R.id.txtname);
        TextView surname = root.findViewById(R.id.txtsur);
        TextView email = root.findViewById(R.id.txtemail);
        TextView birthday = root.findViewById(R.id.txtbirth);


        GetUserHandler handler = new GetUserHandler(getUserId());
        try {
            String res = handler.execute().get();

            ObjectMapper objectMapper = new ObjectMapper();
            User user = objectMapper.readValue(res, User.class);

            name.setText(user.getName());
            surname.setText(user.getSurname());
            email.setText(user.getEmail());
            birthday.setText(formatDate(user.getBirthday()));

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        final Button logOut = root.findViewById(R.id.btn_log_out);
        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logOut();
            }
        });

        //Button List
        final Button editAccount = root.findViewById(R.id.btn_edit);
        editAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = null;
                fragment = new EditAccountFragment();
                replaceFragment(fragment);
            }
        });

        return root;
    }

    private void replaceFragment(Fragment f) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, f);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private int getUserId() {
        SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        return pref.getInt("user_id", -1); // getting Integer
    }

    private void logOut() {
        showSuccess();

        SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.remove("user_id"); // Storing integer
        editor.commit(); // commit changes

        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
    }

    private void showSuccess() {
        Toast.makeText(getActivity().getApplicationContext(), "You have been successfully logged out.", Toast.LENGTH_SHORT).show();
    }

    private String formatDate(String birthday) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date parsedDate = sdf.parse(birthday);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return simpleDateFormat.format(parsedDate);
    }
}
