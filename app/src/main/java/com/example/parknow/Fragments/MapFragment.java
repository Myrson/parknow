package com.example.parknow.Fragments;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.media.Image;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

//import com.android.volley.VolleyLog;
import com.example.parknow.MainActivity;
import com.example.parknow.R;
import com.example.parknow.models.Parking;
import com.example.parknow.models.Reservation;
import com.example.parknow.requests.CurrentReservationHandler;
import com.example.parknow.requests.ReservationHandler;
import com.example.parknow.requests.GetParkingHandler;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.w3c.dom.Text;

import java.util.Map;
import java.util.concurrent.ExecutionException;

import java.io.IOException;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.content.ContentValues.TAG;
//import com.android.volley.VolleyLog;
import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class MapFragment extends Fragment {

    //VARS
    private GoogleMap googleMap;
    MapView mMapView;

    public ArrayList<Parking> parkings;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Get Layout
        View root = inflater.inflate(R.layout.fragment_map, container, false);

        //Initiate Map
        mMapView = (MapView) root.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        GetParkingHandler handler = new GetParkingHandler();
        try {
            boolean isSuccessful = handler.execute().get();
            if (isSuccessful) {
                parkings = handler.parkings;
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        //Markers + Callback object which will be triggered when the instance is ready to be used
        mMapView.getMapAsync(new OnMapReadyCallback() {

            @Override
            public void onMapReady(GoogleMap mMap) {

                //Synch Map
                googleMap = mMap;
                googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

                //Click InfoWindow
                googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        //  startTimer();

                        if (((MainActivity) getActivity()).isCurrentReservation == false) {

                            String userId = String.valueOf(getUserId());

                            Object obj = marker.getTag();
                            Parking p = (Parking) obj;
                            String parkingId = String.valueOf(p.getId());

                            ReservationHandler reservationHandler = new ReservationHandler(userId, parkingId);
                            makeReservationAPI(marker, reservationHandler);


                            String id = getCurrentReservationID();
                            ((MainActivity) getActivity()).startTimer(id);
                        }
                        else {
                            Toast.makeText((MainActivity) getContext(), "Your previous booking has not finished yet.", Toast.LENGTH_LONG).show();
                        }
                    }
                });

                for (Parking parking :
                        parkings) {
                    googleMap.addMarker(new MarkerOptions().
                            icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker))
                            .position(new LatLng(parking.getLatitude(), parking.getLongitude()))
                            .title(parking.getName())
                            .snippet(parking.getName()))
                            .setTag(parking);
                }

                //Custom Info Window
                googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                    @Override
                    public View getInfoWindow(Marker marker) {
                        return null;
                    }

                    //Set all params for the Custom InfoWindow
                    @Override
                    public View getInfoContents(Marker marker) {
                        View v = getLayoutInflater().inflate(R.layout.custom_info_window, null);
                        TextView tv1 = (TextView) v.findViewById(R.id.title);
                        ImageView status = (ImageView) v.findViewById(R.id.status);
                        TextView tv3 = (TextView) v.findViewById(R.id.textslot);
                        TextView tv4 = (TextView) v.findViewById(R.id.space);
                        TextView tv5 = (TextView) v.findViewById(R.id.slots);

                        String title = marker.getTitle();

                        Object obj = marker.getTag();
                        Parking p = (Parking) obj;

                        tv1.setText(title);
                        status.setImageResource(R.drawable.ic_available);
                        tv3.setText("Available Slots: ");
                        tv4.setText(" ");
                        String c = p.getNumberOfFreeSpaces();
                        tv5.setText(p.getNumberOfFreeSpaces());
                        return v;
                    }
                });

                // For zooming automatically to the location of the marker
                LatLng zoom = new LatLng(41.179000, -8.607674);
                CameraPosition cameraPosition = new CameraPosition.Builder().target(zoom).zoom(16).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });
        return root;
    }

    private String  getCurrentReservationID() {
        CurrentReservationHandler currentReservationHandler = new CurrentReservationHandler(getContext());

        CurrentReservationHandler handler2 = new CurrentReservationHandler(getActivity());
        boolean didRes = false;
        try {
            didRes =  handler2.execute(getUserId()).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        if (didRes) {
            String ID = handler2.reservationsHistory.get(handler2.reservationsHistory.size() - 1).getId();
            return ID;
        }
        else return "";
    }

    private void makeReservationAPI(Marker marker, ReservationHandler reservationHandler) {
        String spaceID = "not";
        try {
            spaceID =  reservationHandler.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        if (spaceID == "-1" || spaceID == "-2" || spaceID == "not"  ) {
            Toast.makeText(getActivity(),"Reservation failed.",Toast.LENGTH_LONG).show();

        } else {
            Toast.makeText(getActivity(),"You make a reservation.",Toast.LENGTH_SHORT).show();
            onButtonShowPopupWindowClick(getView(), marker.getTitle(), spaceID);
        }
    }

        /*public void startStop(){
        if(timeRunning){
            stopTimer();
        } else {
            startTimer();
        }
    }*/
/*
    public void startTimer() {
        countDownTimer = new CountDownTimer(timeLeftInMilliseconds, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timeLeftInMilliseconds = millisUntilFinished;
                updateTimer();
            }

            @Override
            public void onFinish() {

                //INSERT BOOKING HERE

//                Log.e(VolleyLog.TAG,"Finish");
                timer.setVisibility(View.INVISIBLE);
                reltimer.setVisibility(View.INVISIBLE);
            }
        }.start();
        timer.setVisibility(View.VISIBLE);
        reltimer.setVisibility(View.VISIBLE);
        //cancelbtn.setVisibility(View.VISIBLE);
    }*/

    /*public void stopTimer(){
        countDownTimer.cancel();
        countdownButton.setText("START");
        timeRunning = false;
    }*/
/*
    public void updateTimer() {
        int minutes = (int) timeLeftInMilliseconds / 60000;
        int seconds = (int) timeLeftInMilliseconds % 60000 / 1000;

        String timeLeftText;

        timeLeftText = "" + minutes;
        timeLeftText += ":";
        if (seconds < 10) timeLeftText += "0";
        timeLeftText += seconds;

        timer.setText(timeLeftText);
    }*/

    //Class needed to exchange Fragment
    public void replaceFragment(Fragment mapFragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, mapFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    public void onButtonShowPopupWindowClick(View view, String parkingTitile, String spaceId) {

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.reservation_confirmation_popup, null);
        TextView textInputName = popupView.findViewById(R.id.parkingNameTxt);
        textInputName.setText(parkingTitile);

        TextView spaceIdName = popupView.findViewById(R.id.spaceIdTxt);
        spaceIdName.setText(spaceId);

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }

    private int getUserId() {
        SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        return pref.getInt("user_id", -1); // getting Integer
    }
}