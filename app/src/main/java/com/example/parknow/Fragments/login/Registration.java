package com.example.parknow.Fragments.login;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.parknow.R;
import com.example.parknow.requests.RegisterUserHandler;

import java.util.Calendar;
import java.util.concurrent.ExecutionException;


public class Registration extends AppCompatActivity {

    private EditText new_name, new_surname, new_email, new_birthday, new_password, new_repeat_password;
    private Button register;
    private DatePickerDialog.OnDateSetListener mDateSetListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        new_name = (EditText) findViewById(R.id.new_name);
        new_surname = (EditText) findViewById(R.id.new_surname);
        new_email = (EditText) findViewById(R.id.new_email);
        new_birthday = (EditText) findViewById(R.id.new_birthday);
        new_password = (EditText) findViewById(R.id.new_password);
        new_repeat_password = (EditText) findViewById(R.id.new_repeat_password);

        new_birthday.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(
                        Registration.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                //Log.d(TAG,"onDateSet: date: " + day + "/" + month + "/" + year);
                month = month + 1;
                String date = month + "/" + day + "/" + year;
                new_birthday.setText(date);
            }
        };


        register = (Button) findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });
    }

    private void submitForm() {
        registerUser(new_name.getText().toString(),
                new_surname.getText().toString(),
                new_email.getText().toString(),
                new_birthday.getText().toString(), // TODO is birthday correct ?
                new_password.getText().toString(),
                new_repeat_password.getText().toString());
    }

    private void registerUser(final String name, final String surname, final String email, final String birthday, final String password, final String rePassword) {

        if(name.isEmpty() || surname.isEmpty() || email.isEmpty() || birthday.isEmpty() || password.isEmpty() || rePassword.isEmpty()) {
            showEmptyFields();
            return;
        }

        if(!password.equals(rePassword)) {
            showDifferentPasswords();
            return;
        }

        RegisterUserHandler handler = new RegisterUserHandler(email, name, surname , birthday, password);
        boolean didCreateUser = false;
        try {
            didCreateUser = handler.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        if (didCreateUser) {
            showSuccess();

            Intent intent = new Intent(Registration.this, LoginActivity.class);
            startActivity(intent);
        } else {
            showFailedRegistration();
        }
    }

    private void showEmptyFields() {
        Toast.makeText(getApplicationContext(), "Fields cannot be empty", Toast.LENGTH_SHORT).show();
    }

    private void showDifferentPasswords() {
        Toast.makeText(getApplicationContext(), "Passwords differ", Toast.LENGTH_SHORT).show();
    }

    private void showFailedRegistration() {
        Toast.makeText(getApplicationContext(), "Creating a new account failed", Toast.LENGTH_SHORT).show();
    }

    private void showSuccess() {
        Toast.makeText(getApplicationContext(), "New account has been created. You can sign in now.", Toast.LENGTH_SHORT).show();
    }

}
