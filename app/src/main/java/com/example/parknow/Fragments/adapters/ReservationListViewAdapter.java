package com.example.parknow.Fragments.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.parknow.Fragments.history.HistoryFragment;
import com.example.parknow.Fragments.login.LoginActivity;
import com.example.parknow.Fragments.login.Registration;
import com.example.parknow.R;
import com.example.parknow.models.Reservation;
import com.example.parknow.requests.ConfrimArrivalHandler;

import java.text.ParseException;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ReservationListViewAdapter extends BaseAdapter {
    private final List<Reservation> items;
    private boolean isCurrent = false;

    public boolean isEffect() {
        return effect;
    }



    private boolean effect = false;

    public ReservationListViewAdapter(List<Reservation> items ) {

        this.items = items;

    }

    public void setIsCurrent() {
        this.isCurrent = true;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int arg0, View arg1, ViewGroup arg2) {
        final Reservation row = this.items.get(arg0);
        View itemView = null;
        if (arg1 == null) {
            LayoutInflater inflater = (LayoutInflater) arg2.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.fragment_history_item, null);
        } else {
            itemView = arg1;
        }

        // Set the text of the row
        final TextView idHistory = (TextView) itemView.findViewById(R.id.idHistory);
        idHistory.setText(row.getId());

        TextView startTimeHistory = (TextView) itemView.findViewById(R.id.startTimeHistory);
        try {
            startTimeHistory.setText(row.getStartTimeString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        TextView parkingIdHistory = (TextView) itemView.findViewById(R.id.parkingIdHistory);
        parkingIdHistory.setText(row.getParkingId());

        TextView spaceIdHistory = (TextView) itemView.findViewById(R.id.spaceIdHistory);
        spaceIdHistory.setText(row.getSpaceId());

        TextView statusHistory = (TextView) itemView.findViewById(R.id.statusHistory);
        statusHistory.setText(row.getStatus());

       /* if(isCurrent){
            Button confirm = (Button) itemView.findViewById(R.id.confirmArrival);
            confirm.setVisibility(View.VISIBLE);
            //Change activity to create an account
            confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ConfrimArrivalHandler confrimArrivalHandler = new ConfrimArrivalHandler(row.getId());

                    boolean didRes = false;
                    try {
                        didRes =  confrimArrivalHandler.execute().get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }

                    if (didRes) {
                        effect = true;
                        Toast.makeText(v.getContext(), "Arrival confirmed.", Toast.LENGTH_LONG).show();
                        Button confirm = (Button) v.findViewById(R.id.confirmArrival);
                        confirm.setVisibility(View.INVISIBLE);
                      //  v.getContext().startActivity(new Intent(v.getContext(), HistoryFragment.class));
                    } else {

                        Toast.makeText(v.getContext(), "Time is up. Reservation fail.", Toast.LENGTH_LONG).show();
                        Button confirm = (Button) v.findViewById(R.id.confirmArrival);
                        confirm.setVisibility(View.INVISIBLE);
                        effect = false;
                    }
                }
            });
        }
*/
        return itemView;
    }


}
