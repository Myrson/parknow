package com.example.parknow.Fragments.history;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.parknow.Fragments.adapters.ReservationListViewAdapter;
import com.example.parknow.R;
import com.example.parknow.models.Reservation;
import com.example.parknow.requests.CurrentReservationHandler;
import com.example.parknow.requests.HistoryReservationHandler;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class HistoryFragment extends Fragment {

    private ArrayList<Reservation> reservationsArray;
    private ArrayList<Reservation> currentReservationsArray;


    private ListView listViewHistoryResrvation = null;
    private ListView listViewCuurentResrvation = null;

    private ReservationListViewAdapter reservationListViewAdapterHistory;
    private ReservationListViewAdapter reservationListViewAdapterCurrent;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        reservationsArray = new ArrayList<Reservation>();
        currentReservationsArray = new ArrayList<Reservation>();

        View root = inflater.inflate(R.layout.fragment_history, container, false);
        listViewHistoryResrvation= (ListView) root.findViewById(R.id.listViewHistory);
        listViewCuurentResrvation= (ListView) root.findViewById(R.id.currentReservationList);

        int userId = getUserId();

        HistoryReservationHandler handler = new HistoryReservationHandler(getActivity());
        boolean didHistroyRes = false;
        try {
            didHistroyRes =  handler.execute(userId).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        if (didHistroyRes) {
            reservationsArray = handler.reservationsHistory;

            if(reservationsArray.isEmpty()){
                TextView emptyHistoryListText = (TextView) root.findViewById(R.id.emptyHistoryListText);
                emptyHistoryListText.setText("You don't have any reservations yet.");
            }else{
                reservationListViewAdapterHistory = new ReservationListViewAdapter(reservationsArray);
                listViewHistoryResrvation.setAdapter(reservationListViewAdapterHistory);
            }

        } else {
            showFailed();
        }


        //current
        CurrentReservationHandler handler2 = new CurrentReservationHandler(getActivity());
        boolean didRes = false;
        try {
            didRes =  handler2.execute(userId).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        if (didRes) {
            currentReservationsArray = handler2.reservationsHistory;

            if(currentReservationsArray.isEmpty()){
                TextView emptyHistoryListText = (TextView) root.findViewById(R.id.emptycurrentReservation);
                emptyHistoryListText.setText("You don't have any current reservation.");
            }else{

                reservationListViewAdapterCurrent = new ReservationListViewAdapter(currentReservationsArray);
                reservationListViewAdapterCurrent.setIsCurrent();
                   //     if (reservationListViewAdapterCurrent.isEffect())

                listViewCuurentResrvation.setAdapter(reservationListViewAdapterCurrent);
            }

        } else {
            showFailed();
        }



        return root;
    }


    private void showFailed() {
        Toast.makeText(getActivity().getApplicationContext(), "fail ", Toast.LENGTH_SHORT).show();
    }

    private int getUserId() {
        SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        return pref.getInt("user_id", -1); // getting Integer
    }
}