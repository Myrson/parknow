package com.example.parknow.models;

import com.example.parknow.requests.GetNumberOfFreeSpacesHandler;
import com.example.parknow.requests.HistoryReservationHandler;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.concurrent.ExecutionException;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Parking {

    private int id;
    private String name;
    private float longitude;
    private float latitude;
    private int numberOfSpaces;
    private int period;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfSpaces() {
        return numberOfSpaces;
    }

    public String getNumberOfFreeSpaces() {

        GetNumberOfFreeSpacesHandler handler = new GetNumberOfFreeSpacesHandler(id);
        String didNumber = "";
        try {
            didNumber =  handler.execute().get();
            return didNumber;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return "0";
    }

    public void setNumberOfSpaces(int numberOfSpaces) {
        this.numberOfSpaces = numberOfSpaces;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }
}