package com.example.parknow.models;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Reservation {

    private String status, spaceId, parkingId, id;
    private Date startTime;
    private Date endTime;

   /* public Reservation(String status, String startTime, String endTime, String spaceId, String parkingId) {
        this.status = status;
        this.startTime = startTime;
        this.endTime = endTime;
        this.spaceId = spaceId;
        this.parkingId = parkingId;
    }*/

    public  Reservation(){

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getParkingId() {
        return parkingId;
    }

    public void setParkingId(String parkingId) {
        this.parkingId = parkingId;
    }

    public String getSpaceId() {
        return spaceId;
    }

    public void setSpaceId(String spaceId) {
        this.spaceId = spaceId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getStartTime() {
        return startTime;
    }

    public String getStartTimeString() throws ParseException {
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
        String formattedDate = outputFormat.format(startTime);
        return formattedDate;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
